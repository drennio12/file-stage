<?php
/*
* Custom metabox management
*/

require_once( 'custom-metabox-support.php' );

// define("prefix", '_esl_');

// Metabox definition functions association to WordPress hook
add_action( 'cmb2_admin_init', 'add_metabox_contatti' );

function add_metabox_contatti() {
	$prefix = '_esl';

	$cmb = new_cmb2_box(
		array(
			'id'			=> $prefix . 'contatti',
			'title'			=> __( 'Contatti e posizione', 'cmb2' ),
			'object_types'	=> array('hotel', 'residence'),
			'show_names'	=> true
		)
	);

	AddFieldToMetabox($cmb, '', 'Priorità', 'priorita', 'text_small');

	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_indirizzo',
		'type'			=> 'group',
		'description'	=> __( 'Indirizzo', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Indirizzo', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Via',
		'id'	=> 'via',
		'type'	=> 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'CAP',
		'id'	=> 'cap',
		'type'	=> 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Citta',
		'id'	=> 'citta',
		'type'	=> 'text'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Prov',
		'id'	=> 'provincia',
		'type'	=> 'text_small'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_consigliato',
		'type'			=> 'group',
		'description'	=> __( 'Dati per mostrare la struttura prima delle altre nelle ricerche', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Consigliato', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Consigliato',
		'id'	=> 'consigliato',
		'type'	=> 'checkbox'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Dal',
		'id'	=> 'dal',
		'type'	=> 'text_date'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Al',
		'id'	=> 'al',
		'type'	=> 'text_date'
	) );

	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_contatti',
		'type'			=> 'group',
		'description'	=> __( 'Dati di contatto', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Contatti', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Telefono',
		'id'	=> 'telefono',
		'type'	=> 'text_small'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Email',
		'id'	=> 'email',
		'type'	=> 'text_email'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Dal',
		'id'	=> 'dal',
		'type'	=> 'text_date'
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Al',
		'id'	=> 'al',
		'type'	=> 'text_date'
	) );

	//add Gmap and position
	$group_field_id = $cmb->add_field( array(
		'id'			=> $prefix . '_posizione',
		'type'			=> 'group',
		'description'	=> __( 'Posizione', 'cmb2' ),
		'repeatable'	=> false,
		'options'		=> array( 'group_title'   => __( 'Posizione', 'cmb2' ) ),
	) );

	// Id's for group's fields only need to be unique for the group. Prefix is not needed.
	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Mappa',
		'id'	=> 'mappa',
		'type'	=> 'gmap',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Indirizzo Gmap',
		'id'	=> 'indirizzo_gmap',
		'type'	=> 'text',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Latitudine',
		'id'	=> 'latitudine',
		'type'	=> 'text',
	) );

	$cmb->add_group_field( $group_field_id, array(
		'name'	=> 'Longitudine',
		'id'	=> 'longitudine',
		'type'	=> 'text',
	) );
}
