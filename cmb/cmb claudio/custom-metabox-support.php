<?php

//support function for CMB2 metaboxes
function AddFieldToMetabox($box, $prefix, $label, $name, $type, $repeatable = false, $previewSize = '', $sanitize_callback = ''){
	$args = array(
		'name' => __( $label, 'cmb2' ),
		'id'   => '_esl_' . $prefix . $name,
		'type' => $type,
		'repeatable' => $repeatable,
		);
	
	if ( $previewSize != ''){
		$args['preview_size'] = $previewSize;
	}

	if ( $sanitize_callback != ''){
		$args['sanitization_cb'] = $sanitize_callback;
	} else {
		$args['sanitization_cb'] = 'sanitize_text_field';
	}

	$box->add_field( $args );
}

add_filter( 'cmb2_sanitize_gmap', 'cmb2_sanitize_map_callback', 10, 2 );
add_action( 'cmb2_render_gmap', 'sm_cmb_render_gmap', 10, 5 );

//removes iframe tag and google classes from Gmap
function cmb2_sanitize_map_callback( $override_value, $value ) {
	// check if is gmaps iframe
	if (  substr ( $value, 0 , 13 ) == '<iframe src="') {
		// remove iframe code
		$value = substr ( $value, 13 , strlen($value) );
		$value = substr ( $value, 0, strpos( $value, '"'));
	}
	return $value;
}

function sm_cmb_render_gmap( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
	echo $field_type_object->input( array( 'class' => 'regular-text', 'type' => 'text' ) );
}

?>