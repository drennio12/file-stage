<?php

//support function for CMB2 metaboxes
function AddFieldToMetabox($box, $prefix, $label, $name, $type, $repeatable = false, $previewSize = ''){
	$box->add_field(
		array(
			'name' => __( $label, 'cmb2' ),
			'id'   => $prefix . '_field_' . $name,
			'type' => $type,
			'repeatable' => $repeatable,
			'preview_size' => $previewSize
		) 
	);
}

add_filter( 'cmb2_sanitize_gmap', 'cmb2_sanitize_map_callback', 10, 2 );
add_action( 'cmb2_render_gmap', 'sm_cmb_render_gmap', 10, 5 );

function cmb2_sanitize_map_callback( $override_value, $value ) {
    // check if is gmaps iframe
    if (  substr ( $value, 0 , 13 ) == '<iframe src="') {
        // remove iframe code
        $value = substr ( $value, 13 , strlen($value) );
		$value = substr ( $value, 0, strpos( $value, '"'));
    }
    return $value;
}

function sm_cmb_render_gmap( $field, $escaped_value, $object_id, $object_type, $field_type_object ) {
    echo $field_type_object->input( array( 'class' => 'regular-text', 'type' => 'text' ) );
}

?>