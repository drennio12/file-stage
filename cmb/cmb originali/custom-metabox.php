<?php
/*
* Custom metabox management
*/

require_once( 'custom-metabox-support.php' );

// Associazione delle funzioni alle azioni di wordpress per la gestione del metabox
add_action( 'cmb2_admin_init', 'categorie_meta_box_taxonomy' );
add_action( 'cmb2_admin_init', 'add_metabox_indirizzo' );
add_action( 'cmb2_admin_init', 'add_metabox_galleria' );
add_action( 'cmb2_admin_init', 'add_metabox_evidenza' );
add_action( 'cmb2_admin_init', 'add_metabox_contatti' );

function categorie_meta_box_taxonomy() {
	$prefix = '_categorie_';

	$cmb_term = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => __( 'Immagine Categoria', 'cmb2' ),
		'object_types'     => array( 'term' ),
		'taxonomies'       => array( 'category', 'categorie-partner' )
	) );

	AddFieldToMetabox($cmb_term, $prefix, 'Immagine Categoria', 'immagine', 'file');
}

function add_metabox_galleria() {
	$prefix = '_custom';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'galleria',
			'title'        => __( 'Galleria', 'cmb2' ),
			'object_types' => array('strutture'),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Galleria', 'galleria', 'file_list', false, array( 100, 100 ));
}


// Funzione per la definizione di un metabox e indicazione della funzione da richiamare
function add_metabox_indirizzo(){
	$prefix = '_custom_';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'indirizzo',
			'title'        => __( 'Indirizzo', 'cmb2' ),
			'object_types' => array('strutture', 'partner' ),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Indirizzo', 'indirizzo', 'text');
	AddFieldToMetabox($cmb, $prefix, 'CAP', 'cap', 'Text');
	AddFieldToMetabox($cmb, $prefix, 'Località', 'localita', 'text');
	AddFieldToMetabox($cmb, $prefix, 'Provincia', 'provincia', 'text');
}

function add_metabox_contatti(){
	$prefix = '_custom_';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'contatti',
			'title'        => __( 'Contatti', 'cmb2' ),
			'object_types' => array('strutture', 'partner' ),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Telefono', 'telefono', 'text');
	AddFieldToMetabox($cmb, $prefix, 'Email', 'email', 'text_email');
	AddFieldToMetabox($cmb, $prefix, 'Dal', 'dal', 'text_date');
	AddFieldToMetabox($cmb, $prefix, 'Al', 'al', 'text_date');
}

function add_metabox_evidenza(){
	$prefix = '_custom_';

	$cmb = new_cmb2_box(
		array(
			'id'           => $prefix . 'evidenza',
			'title'        => __( 'In evidenza', 'cmb2' ),
			'object_types' => array('strutture', 'partner' ),
			'show_names'   => true
		)
	);

	AddFieldToMetabox($cmb, $prefix, 'Priorità', 'priorita', 'text');
	AddFieldToMetabox($cmb, $prefix, 'Inizio', 'inizio', 'text_date');
	AddFieldToMetabox($cmb, $prefix, 'Fine', 'fine', 'text_date');
}

?>